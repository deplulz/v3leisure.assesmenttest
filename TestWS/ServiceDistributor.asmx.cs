﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace V3Leisure.TestWS {
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(
        Namespace = "http://helloworld.org/",
        Description = ""
    )]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    //
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    //
    public class ServiceDistributor : System.Web.Services.WebService {
        // Escaping quotes for safety query
        private string StrEscape(string value) {
            if (value == null) value = "";
            return value.Replace("'", "''");
        }

        [WebMethod]
        // WebMethod: Adding record
        public AddRequestResult Add(string fullName, int status) {
            try {
                Guid newID = Guid.NewGuid();

                if (fullName.Length <= 0) {
                    throw new Exception("Full name cannot be empty");
                } else if (status < 1 || status > 3) {
                    throw new Exception("Status is not valid");
                }

                TestDataSource._dataTable.Rows.Add(newID, fullName, status);
                return new AddRequestResult(newID);
            } catch (Exception ex) {
                return new AddRequestResult(false, ex.Message);
            }
        }

        [WebMethod]
        // WebMethod: Updating record
        public RequestResult Update(string id, string fullName = null, int status = -1) {
            try {
                return this.Update(Guid.Parse(id), fullName, status);
            } catch (Exception ex) {
                return new RequestResult(false, ex.Message);
            }
        }

        // Updating record
        public RequestResult Update(Guid id, string fullName = null, int status = -1) {
            try {
                DataRow[] rows = TestDataSource._dataTable.Select("BODS_Id = '" + this.StrEscape(id.ToString()) + "'");
                if (rows.Length > 0) {
                    if (status < 1 || status > 3) {
                        if (status != -1) throw new Exception("Status not valid");
                    }

                    if (fullName != null) rows[0]["BODS_FullName"] = fullName;
                    if (status != -1) rows[0]["BODS_Status"] = status;

                    return new RequestResult(true, "");
                } else throw new Exception("Record not found");
            } catch (Exception ex) {
                return new RequestResult(false, ex.Message);
            }
        }

        [WebMethod]
        // WebMethod: Fetching record(s)
        public GetRequestResult Get(string mode, string query = null) {
            try {
                DataRow[] rows = null;
                switch (mode.ToUpper()) {
                    case "ID":
                        if (query == null || query == "") throw new Exception("Query cannot be empty");
                        rows = TestDataSource._dataTable.Select("BODS_ID = '" + this.StrEscape((Guid.Parse(query)).ToString()) + "'");
                        break;

                    case "FULLNAME":
                    case "NAME":
                        if (query == null) throw new Exception("Query cannot be null");
                        rows = TestDataSource._dataTable.Select("BODS_FullName LIKE '%" + this.StrEscape(query) + "%'");
                        break;

                    case "STATUS":
                        if (query == null || query == "") throw new Exception("Query cannot be empty");

                        int queryData = int.Parse(query);
                        if (queryData < 1 || queryData > 3) {
                            throw new Exception("Status not valid");
                        }

                        rows = TestDataSource._dataTable.Select("BODS_Status = " + queryData.ToString());
                        break;

                    case "":
                    case "ALL":
                        rows = TestDataSource._dataTable.Select();
                        break;

                    default:
                        throw new Exception("Unknown get mode");
                }
                return new GetRequestResult(rows);

            } catch (Exception ex) {
                return new GetRequestResult(false, ex.Message);
            }
        }
    }
}