﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace V3Leisure.TestWS {
    namespace DataStructures {
        public class Distributor {
            public Guid id         = Guid.Empty;
            public string fullName = null;
            public byte status     = 0;
        }
    }
}